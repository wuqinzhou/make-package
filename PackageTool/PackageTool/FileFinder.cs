﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PackageTool
{
	/// <summary>
	/// 文件简单信息，包含文件名和文件长度
	/// </summary>
	public struct tagMyFileInfo
	{
		//文件名（含路径）
		private string 	m_strFileName;

		public string FileName {
			get {
				return m_strFileName;
			}
			set {
				m_strFileName = value;
			}
		}

		// 文件大小
		private Int32		m_nLength;

		public Int32 Length {
			get {
				return m_nLength;
			}
			set {
				m_nLength = value;
			}
		}
	}

	/// <summary>
	/// 文件查找类
	/// 查找一个目录下的文件，记录文件的长度和相对路径（相对于传入的根目录路径）。并返回统计结果
	/// </summary>
	public class FileFinder
	{
		// 文件过滤器
		private List<FileFilterBase> m_lstFilter;

		// 单例
		private static FileFinder inst;
		public static FileFinder Inst 
		{
			get 
			{
				if (null == inst) 
				{
					inst = new FileFinder ();
				}
				return inst;
			}
		}

		public FileFinder ()
		{
			m_lstFilter = new List<FileFilterBase> ();
		}

		// 增加过滤器
		public void AddFilter(FileFilterBase filter)
		{
			m_lstFilter.Add (filter);
		}

		public void Init()
		{
			m_lstFilter.Clear ();
		}

		//参数dirPath为指定的目录
		public List<tagMyFileInfo> FindFile(string dirPackPath, string strRootPath) 
		{ 
			if ("" == dirPackPath || "" == strRootPath)
			{
				OutputSubject.Inst.OutputInfo( "请输入文件目录", ENotifyLevel.eEL_Error);
				return null;
			}

			// 判断根目录是否存在
			DirectoryInfo dirRoot = new DirectoryInfo(strRootPath);
			if (!dirRoot.Exists) 
			{
				OutputSubject.Inst.OutputInfo(strRootPath + " 目录不存在", ENotifyLevel.eEL_Error);
				return null;
			}

			// 所有的文件名（含路径）
			List<tagMyFileInfo> lstFile = new List<tagMyFileInfo>();

			//在指定目录及子目录下查找文件
			DirectoryInfo dirBase = new DirectoryInfo(dirPackPath);
			if (!dirBase.Exists) 
			{
				OutputSubject.Inst.OutputInfo(dirPackPath + " 目录不存在", ENotifyLevel.eEL_Error);
				return null;
			}

			//查找所有文件
			foreach(FileInfo file in dirBase.GetFiles ("*.*", SearchOption.AllDirectories) ) 
			{
				// 系统文件和隐藏文件不打包
				if ( FileAttributes.System == (file.Attributes & FileAttributes.System) ||
					FileAttributes.Hidden == (file.Attributes & FileAttributes.Hidden) )
				{
					continue;
				}

				bool bAddFile = true;

				// 遍历所有过滤器，有一个过滤器过不去就不添加
				foreach (FileFilterBase filter in m_lstFilter)
				{
					if (filter.IsFileShouldBlock (file))
					{
						bAddFile = false;
						break;
					}
				}

				if (bAddFile)
				{
					//保存文件信息
					AddFileInfo (file, ref lstFile, strRootPath);
				}
			}

			return lstFile;
		}

		// 增加文件信息
		private void AddFileInfo(FileInfo file,ref List<tagMyFileInfo> lstFile, string strRootPath)
		{
			if (file.ToString ().Length + 1 < strRootPath.Length)
			{
				OutputSubject.Inst.OutputInfo ("Error 文件名：" + file.ToString () + " 长度比资源根目录：" + strRootPath + "长", 
					ENotifyLevel.eEL_Error);
				return;
			}

			tagMyFileInfo tagInfo = new tagMyFileInfo();
			// 文件名为打包目录的相地路径
			tagInfo.FileName = file.ToString ().Substring(strRootPath.Length+1);
			// 将目录索引的'\‘换成'/'，保证不同操作系统下目录索引一致
			tagInfo.FileName.Replace ("\\", "/");

			tagInfo.Length = (Int32)file.Length;

			lstFile.Add(tagInfo); 
		}

	}
}


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PackageTool
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			OutputSubject.Inst.AddObserver (new ConsoleObserver ());

			Console.WriteLine ("请输入要打包的资源文件的根目录");
			string strRootPath = Console.ReadLine ();

			Console.WriteLine ("请输入要打包的文件路径数量");
			int nPathNum = Convert.ToInt32( Console.ReadLine () );

			Console.WriteLine ("请输入要打包的文件路径,每行一个:");
			string[] strPaths = new string[nPathNum];
			for (int nNum = 0; nNum < nPathNum; ++nNum) 
			{
				strPaths [nNum] = Console.ReadLine ();
			}

			Console.WriteLine ("请输入打包文件的输出路径（含文件名）：");
			string strOutput = Console.ReadLine ();

			Console.WriteLine ("请输入资源包的版本号：");
			int nVersion = Convert.ToInt32( Console.ReadLine () );

			// 选择过滤器
			SelectFilter ();

			// 初始化打包类
			MakePackage.Inst.InitData ();

			// 设置版本和资源类型
			MakePackage.Inst.Version = nVersion;

			// 统计所有要打包的文件
			List<tagMyFileInfo> lstFileInfo = new List<tagMyFileInfo>();
			foreach(string strPackPath in strPaths)
			{
				List<tagMyFileInfo> lstInfo = FileFinder.Inst.FindFile(strPackPath, strRootPath);
				lstFileInfo.AddRange(lstInfo);
			}

			// 打包所有文件
			MakePackage.Inst.PackageAllFiles(ref lstFileInfo, strOutput, strRootPath);
		}

		// 选择过滤器
		private static void  SelectFilter()
		{
			Console.WriteLine( string.Format("请选择过滤器：\n0.无\n{0:D}.特定后缀过滤器\n{1:D}.排除后缀过滤器\n提示：选择多个时请输入过滤器序号之和", 
				EFilterType.eFT_ExtensionOnly,
				EFilterType.eFT_ExtensionWithout) );
			int nFilterType = Convert.ToInt32( Console.ReadLine () );

			// 特定后缀过滤器
			if ( ((int)EFilterType.eFT_ExtensionOnly & nFilterType) != 0)
			{
				// 初始化文件查找器
				FileFinder.Inst.Init ();

				Console.WriteLine ("请输入要打包的特定后缀格式，以'|'隔开");

				string strInput = Console.ReadLine ();

				// 是否只打包一些特定后缀的文件
				string[] strOnlyExtension = strInput.Split ('|');

				FileFilterByExtensionOnly filter = new FileFilterByExtensionOnly();
				foreach (string strExtension in strOnlyExtension) 
				{
					filter.AddOnlyExtension (strExtension);
				}
				FileFinder.Inst.AddFilter (filter);
			} 

			// 排除后缀过滤器
			if ( ((int)EFilterType.eFT_ExtensionWithout & nFilterType) != 0)
			{
				Console.WriteLine ("请输入要打包的特定后缀格式，以'|'隔开");

				string strInput = Console.ReadLine ();

				// 是否不打包一些特定后缀的文件
				string[] strWithoutExtension = strInput.Split ('|');

				FileFilterByExtensionWithout filter = new FileFilterByExtensionWithout();
				foreach (string strExtension in strWithoutExtension) 
				{
					filter.AddWithoutExtension (strExtension);
				}
				FileFinder.Inst.AddFilter (filter);
			}
		}

	}
}

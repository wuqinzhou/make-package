﻿using System;
using System.Collections.Generic;

namespace PackageTool
{
	/// <summary>
	/// 通知级别
	/// </summary>
	public enum ENotifyLevel
	{
		// 日志
		eEL_Log,
		// 提示
		eEL_Warning,
		// 错误
		eEL_Error,
	}


	/// <summary>
	/// 输出主题，被观察的对象
	/// </summary>
	public class OutputSubject
	{
		// 观察者
		private List<OutputObserver> m_lstObsever;

		// 单例
		private static OutputSubject inst;
		public static OutputSubject Inst 
		{
			get 
			{
				if (null == inst) 
				{
					inst = new OutputSubject ();
				}
				return inst;
			}
		}

		public OutputSubject ()
		{
			m_lstObsever = new List<OutputObserver> ();
		}

		// 添加观察者
		public void AddObserver(OutputObserver observer)
		{
			m_lstObsever.Add(observer);
		}

		// 移除观察者
		public void RemoveObserver(OutputObserver observer)
		{
			m_lstObsever.Remove (observer);
		}

		// 输出信息
		public void OutputInfo(string strOutput, ENotifyLevel eLevel)
		{
			foreach (OutputObserver observer in m_lstObsever)
			{
				observer.ProcessOutput (strOutput, eLevel);
			}
		}
	}
}


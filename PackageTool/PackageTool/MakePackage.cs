﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace PackageTool
{
	/// <summary>
	/// 打包类
	/// 根据传进来的文件信息，将所有文件打成一个包。
	/// 文件写入时8字节对齐
	/// 
	/// 文件结构为：
	/// int(版本) 0000 int(文件信息长度) 0000
	/// int(单条文件信息长度) 0000 short(文件名长度）string(文件名) int(文件长度) long(文件内容位置) + n个0(对齐)
	/// ...
	/// 文件内容 + n个0(对齐)
	/// ...
	/// 
	/// </summary>
	public class MakePackage
	{
		// 所有的文件名（含路径）
		private List<tagMyFileInfo> m_lstFile;

		// 打包文件的根目录，所有打包文件必需在这个目录下。文件存的是相对这个目录的路径
		private string m_strRootDir;

		// 版本号
		private Int32 m_nVersion;
		public Int32 Version {
			get {
				return m_nVersion;
			}
			set {
				m_nVersion = value;
			}
		}

		// 单例
		private static MakePackage inst;
		public static MakePackage Inst 
		{
			get 
			{
				if (null == inst) 
				{
					inst = new MakePackage ();
				}
				return inst;
			}
		}

		// 构造函数
		private MakePackage ()
		{
			m_lstFile = new List<tagMyFileInfo> ();
			InitData ();
		}

		// 打包所有文件，类入口函数
		public void PackageAllFiles(ref List<tagMyFileInfo> lstFileInfo, string strOutput, string strRootPath)
		{
			m_lstFile = lstFileInfo;

			m_strRootDir = strRootPath;

			MakeResFile (strOutput);
		}

		// 初始化数据
		public void InitData()
		{
			m_lstFile.Clear();
			m_strRootDir = "";
			m_nVersion = 1;
		}

		// 创建资源包文件
		private void MakeResFile(string strOutputPath)
		{
			// 在打包目录的父录目创建同名文件。后缀为.flhs
			using (FileStream fsResFile = File.Create (strOutputPath + ".flhs"))
			{
				if (!fsResFile.CanWrite)
				{
					return;
				}

				// 保存文件信息前面部分
				SaveDataBeforeFileInfo (fsResFile);

				// 保存文件信息
				SaveFileInfo (fsResFile);

				// 保存文件内容
				SaveFileContent (fsResFile);

				// 关闭文件流
				fsResFile.Close ();
			}
		}

		// 保存文件信息前面部分
		private void SaveDataBeforeFileInfo(FileStream fsResFile)
		{
			// 保存版本号
			WriteByteDataToFile( BitConverter.GetBytes(m_nVersion), fsResFile);

			// 保存文件头大小
			WriteByteDataToFile ( BitConverter.GetBytes(GetFileInfosLength ()), fsResFile);
		}

		// 保存文件信息
		private void SaveFileInfo(FileStream fsResFile)
		{
			// 文件内容前面的大小
			Int64 nOffset = GetResFileLenBeforContent ();

			// 写每个文件的信息
			foreach (tagMyFileInfo tagInfo in m_lstFile) 
			{
				// 计算这条文件信息长度
				int nFileInfoDataLen = 2 + tagInfo.FileName.Length + 4 + 8;
					
				// 写文件信息长度
				WriteByteDataToFile (BitConverter.GetBytes (AlignNumberForEight (nFileInfoDataLen)), fsResFile);

				// 文件名长度
				byte[] byNameLen = BitConverter.GetBytes( (short)tagInfo.FileName.Length);
					
				// 文件名
				byte[] byName = Encoding.UTF8.GetBytes(tagInfo.FileName);

				// 文件大小
				byte[] byFileLen = BitConverter.GetBytes(tagInfo.Length);

				// 文件内容开始位置
				byte[] byOffset = BitConverter.GetBytes (nOffset);

				// 把文件信息转换一个字节数组，一起写入文件
				byte[] byFileInfo = new byte[nFileInfoDataLen];
				Array.ConstrainedCopy (byNameLen,	0, byFileInfo, 0, byNameLen.Length);
				Array.ConstrainedCopy (byName,	 	0, byFileInfo, byNameLen.Length, byName.Length);
				Array.ConstrainedCopy (byFileLen,	0, byFileInfo, byNameLen.Length + byName.Length, byFileLen.Length);
				Array.ConstrainedCopy (byOffset, 	0, byFileInfo, byNameLen.Length + byName.Length + byFileLen.Length, byOffset.Length);

				// 写文件信息
				WriteByteDataToFile (byFileInfo, fsResFile);

				// 计算偏移
				nOffset += AlignNumberForEight(tagInfo.Length);
			}
		}

		// 保存文件内容
		private void SaveFileContent(FileStream fsResFile)
		{
			// 查找所有文件
			foreach (tagMyFileInfo tagInfo in m_lstFile)
			{
				// 保存的是相对路径，要加上打包目录的路径
				using (FileStream fsFile = File.OpenRead (m_strRootDir + "/" + tagInfo.FileName)) 
				{
					if (!fsFile.CanRead)
					{
						return;
					}

					// 读取要打包文件的内容
					byte[] byFile = new byte[tagInfo.Length];
					fsFile.Read (byFile, 0, byFile.Length);

					// 保存内容
					WriteByteDataToFile (byFile, fsResFile);

					// 关闭文件流
					fsFile.Close ();
				}

			}
		}

		// 资源文件在内容前的大小
		private Int32 GetResFileLenBeforContent ()
		{
			// 文件版本 + 文件信息长度 + 文件信息
			return 8 + 8 + AlignNumberForEight(GetFileInfosLength ());
		}

		// 文件信息的大小
		private Int32 GetFileInfosLength()
		{
			Int32 nLength = 0;

			// 计算每个文件文件信息的长度
			foreach (tagMyFileInfo tagInfo in m_lstFile)
			{
				// 每条文件信息分为两部分 1.长度8 2.内容(short+string+int+long)
				nLength += (8 + AlignNumberForEight(2 + tagInfo.FileName.Length + 4 + 8) );
			}

			return nLength;
		}

		// 将字节数据写到文件中
		private void WriteByteDataToFile(byte[] byData, FileStream fsResFile)
		{
			// 如果数据长度不是8的倍数
			if (byData.Length % 8 != 0) 
			{
				Array.Resize(ref byData, AlignNumberForEight (byData.Length));
				for (int nIndex = byData.Length; nIndex < AlignNumberForEight (byData.Length); ++nIndex)
				{
					byData [nIndex] = 0;
				}
			}
			fsResFile.Write (byData, 0, byData.Length);
		}

		// 返回与8对齐的数字
		// 用于文件输入时将内容长度调整为8的倍数
		private int AlignNumberForEight(int nInput)
		{
			return (int)(Math.Ceiling ((double)nInput / 8) * 8);
		}
			

	}
}
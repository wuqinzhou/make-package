﻿using System;
using System.IO;

namespace PackageTool
{
	// 过滤器类型
	enum EFilterType
	{
		// 只打包特定后缀
		eFT_ExtensionOnly 		= 1,
		// 排除特定后缀
		eFT_ExtensionWithout	= 2,
	}

	public class FileFilterBase
	{
		public FileFilterBase ()
		{
		}

		public virtual void Init()
		{
		}

		// 是否阻止该文件
		public virtual bool IsFileShouldBlock(FileInfo file)
		{
			return false;
		}
	}
}


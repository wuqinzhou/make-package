﻿using System;

namespace PackageTool
{
	/// <summary>
	/// 控制台观察者，将输出信息输出到控制台
	/// </summary>
	public class ConsoleObserver:OutputObserver
	{
		public ConsoleObserver ()
		{
		}

		public override void ProcessOutput(string strOutput, ENotifyLevel eLevel)
		{
			// 输入信息加上标志
			if (ENotifyLevel.eEL_Log == eLevel)
			{
				strOutput.Insert (0, "Log: ");
			} 
			else if (ENotifyLevel.eEL_Warning == eLevel) 
			{
				strOutput.Insert (0, "Warning: ");
			} 
			else if (ENotifyLevel.eEL_Error == eLevel)
			{
				strOutput.Insert (0, "Error: ");
			}

			Console.WriteLine (strOutput);

			// 是错误直接退出程序
			if (ENotifyLevel.eEL_Error == eLevel) 
			{
				System.Environment.Exit (0);
			}
		}
	}
}


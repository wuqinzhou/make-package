﻿using System;

namespace PackageTool
{
	/// <summary>
	/// 输出观察者，观察及处理输出
	/// </summary>
	public class OutputObserver
	{
		public OutputObserver ()
		{
		}

		public virtual void ProcessOutput(string strOutput, ENotifyLevel eLevel)
		{}
	}
}

